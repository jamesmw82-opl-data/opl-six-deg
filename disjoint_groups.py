
import sys

from opl_sixdeg import load_meets, load_lifters, map_entries,\
                       get_adjacent_entries

if __name__ == '__main__':

    root_lifter_name = sys.argv[1]

    #DEBUG
    print("loading database")

    meets = load_meets('meets.csv')
    (lifters, lifter_name_id_dict,) = load_lifters('lifters.csv')
    (entries_by_meet, entries_by_lifter,) = map_entries('entries.csv', len(meets), len(lifters))

    root_lifter_id = lifter_name_id_dict[root_lifter_name]

    #DEBUG
    print("getting root-adjacent entries")

    root_adj_lifter_set = set()
    root_adj_entry_info_list = [None] * len(lifters)

    for (adj_lifter_id, adj_meet_id,) in get_adjacent_entries(root_lifter_id, entries_by_lifter, entries_by_meet):
        root_adj_lifter_set.add(adj_lifter_id) 
        adj_meet_str = meets[adj_meet_id]

        if not root_adj_entry_info_list[adj_lifter_id]:
            root_adj_entry_info_list[adj_lifter_id] = []

        root_adj_entry_info_list[adj_lifter_id].append(adj_meet_str)

    adj_lifter_disj_set_list = [set()] * len(lifters)

    #DEBUG
    print("getting 2nd order adjacent and non-adjacent lifters")

    # get the set of adjacent lifters for each root-adj lifter, excluding the root lifter themselves
    for adj_lifter_id in root_adj_lifter_set:
        adj2_lifter_id_set = set([
            adj2_lifter_id for (adj2_lifter_id, adj2_meet_id,) in get_adjacent_entries(adj_lifter_id, entries_by_lifter, entries_by_meet) if adj2_lifter_id != root_lifter_id
        ])
        adj_lifter_disj_set_list[adj_lifter_id] = root_adj_lifter_set.difference(adj2_lifter_id_set)

    #DEBUG
    print("Computing and sorting stats")

    # count how often each root-adj lifter appears in sets of lifters disjoint from
    # other root-adj lifters
    # use a dict here because sorting will mess up our index = lifter_id assumption
    disj_count_dict = {}
    for adj_lifter_id in root_adj_lifter_set:
        for disj_lifter_id in adj_lifter_disj_set_list[adj_lifter_id]:

            if not disj_count_dict.get(disj_lifter_id):
                disj_count_dict[disj_lifter_id] = 0
 
            disj_count_dict[disj_lifter_id] += 1              

    sorted_disj_count_items_list = sorted(disj_count_dict.items(), key=lambda item_tup: item_tup[1], reverse=True)
     
    #DEBUG
    print("Showing stats")
    for (lifter_id, disj_count,) in sorted_disj_count_items_list:
        if disj_count:
            print("{} is root-adjacent via {}, and is not adjacent to {} other root-adjacent lifters".format(lifters[lifter_id], root_adj_entry_info_list[lifter_id], disj_count)) 

from csv import DictReader
from queue import Queue

# for now, assume the nth record in meets and lifters has ID n

def load_meets(meets_file):
    meets = []

    with open(meets_file, 'r') as f:
        for meet_line in DictReader(f):
            meets.append("{}-{}-{}".format(meet_line['Date'], meet_line['Federation'], meet_line['MeetName']))

    return meets


def load_lifters(lifters_file):
    lifters = []
    lifter_name_id_dict = {}

    with open(lifters_file, 'r') as f:
        for lifter_line in DictReader(f):
            lifters.append(lifter_line['Name'])
            lifter_name_id_dict[lifter_line['Name']] = int(lifter_line['LifterID'])

    return (lifters, lifter_name_id_dict,)


def map_entries(entries_file, num_meets, num_lifters):
    entries_by_meet = [None] * num_meets
    entries_by_lifter = [None] * num_lifters

    with open(entries_file, 'r') as f:
        for entry_line in DictReader(f):

            meet_id = int(entry_line['MeetID'])
            lifter_id = int(entry_line['LifterID'])

            if entries_by_meet[meet_id]:
                entries_by_meet[meet_id].append(lifter_id)
            else:
                entries_by_meet[meet_id] = [lifter_id]

            if entries_by_lifter[lifter_id]:
                entries_by_lifter[lifter_id].append(meet_id)
            else:
                entries_by_lifter[lifter_id] = [meet_id]

    return (entries_by_meet, entries_by_lifter,)


def get_adjacent_entries(lifter_id, entries_by_lifter, entries_by_meet):

    adj_lifter_meet_ids = []

    for meet_id in entries_by_lifter[lifter_id]:
        for cur_lifter_id in entries_by_meet[meet_id]:
            if cur_lifter_id != lifter_id:
                adj_lifter_meet_ids.append((cur_lifter_id, meet_id,))

    return adj_lifter_meet_ids


def search_lifters(root_lifter_id, leaf_lifter_id, entries_by_lifter, entries_by_meet, lifters, meets):

    seen_lifter_ids = set()
    adj_list = [None] * len(lifters)

    #FIFO
    q = Queue()

    q.put(root_lifter_id)
    seen_lifter_ids.add(root_lifter_id)

    found_leaf = False

    while not q.empty() and not found_leaf:
        cur_lifter_id = q.get()

        if cur_lifter_id == leaf_lifter_id:
            found_leaf = True

        else:
            adj_tup_list = get_adjacent_entries(cur_lifter_id, entries_by_lifter, entries_by_meet)
            for (lifter_id, meet_id,) in adj_tup_list:
                if lifter_id not in seen_lifter_ids:
                    adj_list[lifter_id] = (cur_lifter_id, meet_id,)

                    # add to seen list now because we can have multiple adjacencies between lifters
                    seen_lifter_ids.add(lifter_id)
                    q.put(lifter_id)

                    # if we see the leaf lifter now, don't bother queueing any more siblings
                    if lifter_id == leaf_lifter_id:
                        break
    if not found_leaf:
        raise ValueError("{} and {} are not connected".format(lifters[root_lifter_id], lifters[leaf_lifter_id]))

    return adj_list


def gen_traceback_adj_list(adj_list, root_lifter_id, leaf_lifter_id, lifters, meets):

    cur_lifter_id = leaf_lifter_id

    while cur_lifter_id != root_lifter_id:
        (parent_lifter_id, meet_id,) = adj_list[cur_lifter_id]
        
        yield (cur_lifter_id, parent_lifter_id, meet_id,)
        cur_lifter_id = parent_lifter_id


import sys

from opl_sixdeg import load_meets, load_lifters, map_entries, search_lifters,\
                       gen_traceback_adj_list

if __name__ == '__main__':

    root_lifter_name = sys.argv[1]
    leaf_lifter_name = sys.argv[2]
    

    meets = load_meets('meets.csv')
    (lifters, lifter_name_id_dict,) = load_lifters('lifters.csv')
    (entries_by_meet, entries_by_lifter,) = map_entries('entries.csv', len(meets), len(lifters))
    root_lifter_id = lifter_name_id_dict[root_lifter_name]
    leaf_lifter_id = lifter_name_id_dict[leaf_lifter_name]
    adj_list = search_lifters(root_lifter_id, leaf_lifter_id, entries_by_lifter, entries_by_meet, lifters, meets)

    for (cur_lifter_id, parent_lifter_id, meet_id,) in gen_traceback_adj_list(adj_list, root_lifter_id, leaf_lifter_id, lifters, meets):
        print("{} and {} in {}".format(lifters[cur_lifter_id], lifters[parent_lifter_id], meets[meet_id]))


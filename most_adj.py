from opl_sixdeg import load_meets, load_lifters, map_entries,\
                       get_adjacent_entries


if __name__ == '__main__':
    (lifters, lifter_name_id_dict,) = load_lifters('lifters.csv')
    meets = load_meets('meets.csv')
    (entries_by_meet, entries_by_lifter,) = map_entries('entries.csv', len(meets), len(lifters))

    lifter_num_adj_dict = {}

    for (lifter_id, lifter_name,) in enumerate(lifters):
        adj_lifter_set = set()
        adj_entries = get_adjacent_entries(lifter_id, entries_by_lifter, entries_by_meet)

        for (adj_lifter_id, adj_meet_id,) in adj_entries:
            adj_lifter_set.add(adj_lifter_id)

        lifter_num_adj_dict[lifters[lifter_id]] = len(adj_lifter_set)

    sorted_lifter_num_adj_tup_list = sorted(lifter_num_adj_dict.items(), key=lambda item_tup: item_tup[1], reverse=True)

    for (i, (lifter_name, num_adj,),) in enumerate(sorted_lifter_num_adj_tup_list):
        print("{:-6} {:30} {}".format((i + 1), lifter_name, num_adj))

